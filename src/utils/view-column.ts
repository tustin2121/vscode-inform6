import * as vscode from "vscode"

// This is mainly used to convert from the `openStoryColumn` setting into a ViewColumn.
/**
 * Convert a string to a `ViewColumn`, e.g. `"one"` into `ViewColumn.One` or `"beside"` into `ViewColumn.Beside`.
 *
 * If the string does not correspond to a `ViewColumn`, then `ViewColumn.Active` is returned.
 *
 * @param s - The string to convert.
 */
export function stringToViewColumn(s: string) {
	switch (s) {
		case "one":
			return vscode.ViewColumn.One
		case "two":
			return vscode.ViewColumn.Two
		case "three":
			return vscode.ViewColumn.Three
		case "four":
			return vscode.ViewColumn.Four
		case "five":
			return vscode.ViewColumn.Five
		case "six":
			return vscode.ViewColumn.Six
		case "seven":
			return vscode.ViewColumn.Seven
		case "eight":
			return vscode.ViewColumn.Eight
		case "nine":
			return vscode.ViewColumn.Nine
		case "beside":
			return vscode.ViewColumn.Beside
		default:
			return vscode.ViewColumn.Active
	}
}
